package com.example.calculate;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;


public class MainActivity extends AppCompatActivity {

    public String resSoFar; //apreekjinaatais rezultaats, kaa string
    public int resSoFarInt; //res so far, int
    public String inSoFar;  //izteiksme tik taal
    public String secSoFar = "0"; //otrais skaitlis tik taal
    Button button0, button1, button2, button3, button4, button5, button6,
            button7, button8, button9, buttonadd, buttonsub, buttondiv,
            buttonmul, buttonc, buttoneq, buttonMs, buttonMr, buttonMc;
    EditText textRes, textTest;
    float mValueOne, mValueTwo;
    boolean adder, subber, multier, divver, anyer;

    public String getSecSoFar() {
        return secSoFar;
    }

    public void setSecSoFar(String secSoFar) {
        this.secSoFar = secSoFar;
    }


    public String getResSoFar() {
        //
        if (resSoFar != null)
//        if (!resSoFar.isEmpty())
        {
            int y = (int) Float.parseFloat(resSoFar);
            return Integer.toString(y);
        } else {
            return resSoFar;
        }


    }

    public void setResSoFar(String resSoFar) {
        this.resSoFar = resSoFar;
    }

    public void setResSoFar(int x) {
        this.resSoFar = Integer.toString(x);
    }

    public int getResSoFarInt() {
        return Integer.valueOf(resSoFar);
    }

    public String getInSoFar() {
        return inSoFar;
    }

    public void setInSoFar(String inSoFar) {
        this.inSoFar = inSoFar;
    }

    public void addPress(int a) {
        if (getInSoFar() != null) {
            setInSoFar(getInSoFar() + a);//papildina izteiksmi
        } else {
            setInSoFar(Integer.toString(a));
        }
        if (getSecSoFar() != null) {
            setSecSoFar(getSecSoFar() + a);//papildina otro
        } else {
            setSecSoFar(Integer.toString(a));
        }
        if (!anyer) {
            if (getResSoFar() != null) {
                setResSoFar(getResSoFar() + a);//papildina otro
            } else {
                setResSoFar(Integer.toString(a));
            }
        }


        textRes.setText(getInSoFar());
        textTest.setText(getResSoFar());
        mValueTwo = Float.parseFloat(getSecSoFar());


    }


    public void addPress(String a) {

        anyer = true; //peedeejais nebija skaitlis

        setSecSoFar("");
        if (getInSoFar() == null) {
            setResSoFar("0");
            setInSoFar("0" + a);
            mValueOne = 0;

        } else {
            calcRes();
            mValueOne = Float.parseFloat(getResSoFar() + "");
            setSecSoFar("");
            mValueTwo = 0;
            setInSoFar(getInSoFar() + a);

        }
        textRes.setText(getInSoFar());

        textTest.setText(getResSoFar());//+""+Float.toString(mValueOne)+""+Float.toString(mValueTwo));


    }

    public void calcRes() {
        if (adder) {
            setResSoFar((mValueOne + mValueTwo) + "");
            setSecSoFar("");
            // setInSoFar(getInSoFar() + getResSoFar());
            mValueOne = Float.parseFloat(getResSoFar());
            adder = false;
            //textRes.setText(getInSoFar());
            textTest.setText(getResSoFar());
        }

        if (subber) {
            setResSoFar((mValueOne - mValueTwo) + "");
            setSecSoFar("");
            // setInSoFar(getInSoFar() + getResSoFar());
            mValueOne = Float.parseFloat(getResSoFar());
            subber = false;
            // textRes.setText(getInSoFar());
            textTest.setText(getResSoFar());
        }

        if (multier) {
            setResSoFar((mValueOne * mValueTwo) + "");
            setSecSoFar("");
            //  setInSoFar(getInSoFar() + getResSoFar());
            mValueOne = Float.parseFloat(getResSoFar());
            multier = false;
            //textRes.setText(getInSoFar());
            textTest.setText(getResSoFar());
        }

        if (divver) {
            if (getSecSoFar() == "0") {
                setResSoFar("0");
                setInSoFar("0");
                textRes.setText(getInSoFar());
                textTest.setText(getResSoFar());
            } else
                setResSoFar(mValueOne / mValueTwo + "");
            setSecSoFar("");
            // setInSoFar(getInSoFar() + getResSoFar());
            mValueOne = Float.parseFloat(getResSoFar());
            divver = false;
            // textRes.setText(getInSoFar());
            textTest.setText(getResSoFar());
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button0 = findViewById(R.id.button0);
        button1 = findViewById(R.id.button1);
        button2 = findViewById(R.id.button2);
        button3 = findViewById(R.id.button3);
        button4 = findViewById(R.id.button4);
        button5 = findViewById(R.id.button5);
        button6 = findViewById(R.id.button6);
        button7 = findViewById(R.id.button7);
        button8 = findViewById(R.id.button8);
        button9 = findViewById(R.id.button9);
        buttonadd = findViewById(R.id.buttonadd);
        buttonsub = findViewById(R.id.buttonsub);
        buttonmul = findViewById(R.id.buttonmul);
        buttondiv = findViewById(R.id.buttondiv);
        buttonc = findViewById(R.id.buttonc);
        buttoneq = findViewById(R.id.buttoneq);
        buttonMc = findViewById(R.id.buttonmc);
        buttonMr = findViewById(R.id.buttonmr);
        buttonMs = findViewById(R.id.buttonms);
        textRes = findViewById(R.id.edt1);
        textTest = findViewById(R.id.textTest);


        buttonMs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String str = textTest.getText().toString();
                OutputStreamWriter oRaxt = null;
                try {
                    FileOutputStream fOut = openFileOutput("txtdatne.txt", MODE_PRIVATE);
                    oRaxt = new OutputStreamWriter(fOut);
                    oRaxt.write(str);
                    oRaxt.flush();
                    oRaxt.close();
                    textTest.setText("");
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
        });

        buttonMr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                InputStreamReader iLas = null;
                try {
                    FileInputStream fIn = openFileInput("txtdatne.txt");
                    iLas = new InputStreamReader(fIn);
                    char[] lasBuferis = new char[1];
                    String str = "";
                    int chrLasiti;
                    while ((chrLasiti = iLas.read(lasBuferis)) > 0) {
                        String lasStr = String.copyValueOf(lasBuferis, 0, chrLasiti);
                        str += lasStr;
                        lasBuferis = new char[1];
                    }
                    textTest.setText(str);
                    addPress(Integer.parseInt(str));
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });
        buttonMc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OutputStreamWriter oRaxt = null;
                String str = "";
                try {
                    FileOutputStream fOut = openFileOutput("txtdatne.txt", MODE_PRIVATE);
                    oRaxt = new OutputStreamWriter(fOut);
                    oRaxt.write(str);
                    oRaxt.flush();
                    oRaxt.close();
                    textTest.setText("");
                } catch (
                        FileNotFoundException e) {
                    e.printStackTrace();
                } catch (
                        IOException e) {
                    e.printStackTrace();
                }
            }

        });


        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int b = 1;
                addPress(b);
                // textRes.setText(textRes.getText() + "1");
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int b = 2;
                addPress(b);
            }
        });

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int b = 3;
                addPress(b);
            }
        });

        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int b = 4;
                addPress(b);
            }
        });

        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int b = 5;
                addPress(b);
            }
        });

        button6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int b = 6;
                addPress(b);
            }
        });

        button7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int b = 7;
                addPress(b);
            }
        });

        button8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int b = 8;
                addPress(b);
            }
        });

        button9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int b = 9;
                addPress(b);
            }
        });

        button0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int b = 0;
                addPress(b);
            }
        });

        buttonadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String d = "+";

                addPress(d);
                adder = true;


            }
        });

        buttonsub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String d = "-";

                addPress(d);
                subber = true;

            }
        });

        buttonmul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String d = "*";

                addPress(d);
                multier = true;


            }
        });

        buttondiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String d = "*";

                addPress(d);
                divver = true;
            }
        });


        buttoneq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // mValueTwo = Float.parseFloat(textRes.getText() + "");
                String d = "=";
                addPress(d);
                calcRes();
                setInSoFar(getInSoFar() + getResSoFar());
                textRes.setText(getInSoFar());


            }
        });

        buttonc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResSoFar("0");
                setInSoFar("");
                setSecSoFar("");
                anyer = false;
                adder = false;
                subber = false;
                multier = false;
                mValueOne = 0;
                mValueTwo = 0;

                textRes.setText(getInSoFar());
                textTest.setText("");
            }
        });


    }
}
